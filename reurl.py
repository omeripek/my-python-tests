#!/usr/bin/env python3

import urllib.request
import re

try:
	site = input("Site Adresi: ")
	full_url = "http://" + site
	
	html = urllib.request.urlopen(full_url)
	html = html.read()
	html = str(html)

	#Regex
	tanim = "<b>(.*?)</b>"

	ara = re.search(tanim, html)

	if ara:
		etiket = ara.group(0)
		icerik = ara.group(1)
		
		print("Etiket: " + etiket)
		print("İçerik: " + icerik)
	else:
		print("Hata")
except:

	print("bilinmeyen hata")
