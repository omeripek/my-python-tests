#!/usr/bin/env python3

import smtplib
from email.mime.text import MIMEText

smtp_url = "smtp-mail.outlook.com"
smtp_port = 587
from_to = "mail adresi"
passwd = "parola"

mail_to = input("Kime e-mail Atılacak: ")

mailTo = [mail_to]

subject = " Deneme Mail - terminal"

message = """

Python ile deneme yapıyoruz! 

<b> Kalın Olacak</b>

"""

try:

	mail = MIMEText(message, "html", "utf-8")

	mail["From"] = from_to

	mail["Subject"] = subject

	mail["To"] = ",".join(mailTo)

	mail = mail.as_string()

	print("Lütfen Bekleyiniz...")

	s = smtplib.SMTP(smtp_url,smtp_port)

	s.starttls()
	s.login(from_to, passwd)

	s.sendmail(from_to, mailTo, mail)
	print("Mail gönderildi")

except Exception as e:
	print(str(e))
