#!/usr/bin/env python3

import urllib.request
import urllib.parse

site = input("Site: ")

full_url = "http://" + site

header_infos = {
	'User-Agent' : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36'
}

datas = {
	"username" : "demo",
	"pass"     : "passwd",
	"email"    : "info@0mer.xyz",
	"name"     : "tst user"
}

params = urllib.parse.urlencode(datas)
params = params.encode('utf-8')

reqs = urllib.request.Request(full_url, data = params, headers = header_infos)

html = urllib.request.urlopen(reqs)

print(html.read())
