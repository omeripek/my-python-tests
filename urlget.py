#!/usr/bin/env python3

import urllib.request

site = input("Site Adresi: ")

header_infos = {
	'User-Agent' : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36'
}

request_web = urllib.request.Request(site, headers = header_infos)

html = urllib.request.urlopen(request_web)

print(html.read())