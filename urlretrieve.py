#!/usr/bin/env python3

import urllib.request

site = input("Site: ")

full_url = "http://" + site

urllib.request.urlretrieve(full_url, "favicon.ico")
